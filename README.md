# Computational Study of Glycerol Binding within the Active Site of Coenzyme B12Dependent Diol Dehydratase

Supporting Data 
- geometries for NVT molecular dynamics runs (topology present)
- optimized ONIOM geometries (topology present)